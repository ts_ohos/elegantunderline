# ElegantUnderline

## 项目介绍
本项目是基于开源项目[elegant-underline](https://github.com/romainguy/elegant-underline) 进行harmonyos化的移植和开发的。  
ElegantUnderline 是一个展示如何创建更好的下划线文本装饰。  

#### 项目名称：elegantunderline
#### 所属系列：harmonyos的第三方组件适配移植

#### 功能：一个适用于harmonyos的下划线文本装饰
#### 项目移植状态：部分移植
#### 调用差异：由于缺少文本轮廓与下划线路径相交以对其进行剪辑API，因此功能只实现了部分下划线剪切
#### 原项目GitHub地址：https://github.com/romainguy/elegant-underline

## 支持功能
* 进入首页页面显示正常

## 安装教程

#### 方案一  
开发者在自己的项目中添加依赖  
```
  //核心引入 
  implementation project(':library')
```
#### 方案二
项目根目录的build.gradle中的repositories添加：
```
    buildscript {
       repositories {
           ...
           mavenCentral()
       }
       ...
   }
   
   allprojects {
       repositories {
           ...
           mavenCentral()
       }
   }
```

module目录的build.gradle中dependencies添加：
```
implementation 'com.gitee.ts_ohos:elegantunderline:1.0.0'
```

## 使用说明

#### 代码使用  

##### xml，引用动画组件。
````xml
     <com.ohos.elegantunderline.UnderlineView
            ohos:id="$+id:customcomponent0"
            ohos:height="200"
            ohos:width="match_parent"
            ohos:alignment="center"
            custom:attr_text="High-quality (path)"
            custom:attr_family="sans-serif">
        </com.ohos.elegantunderline.UnderlineView>
    
        <com.ohos.elegantunderline.UnderlineView
            ohos:id="$+id:customcomponent1"
            ohos:height="200"
            ohos:width="match_parent"
            ohos:alignment="center"
            custom:attr_text="High-quality (region)"
            custom:attr_family="sans-serif">
        </com.ohos.elegantunderline.UnderlineView>
        <com.ohos.elegantunderline.UnderlineView
            ohos:id="$+id:customcomponent2"
            ohos:height="200"
            ohos:width="match_parent"
            ohos:alignment="center"
            custom:attr_text="Elegant, practical, high-quality (path)"
            custom:attr_family="sans-serif-condensed">
        </com.ohos.elegantunderline.UnderlineView>
````

## 效果展示
![输入图片说明](images/effect.png)

## 版本迭代
v1.0.0 基于原项目最新版本，初次提交。

## License
Copyright 2015 Danylo Volokh

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file

except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the

License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,

either express or implied. See the License for the specific language governing permissions and limitations under the License.