# ElegantUnderline
本组件基于原库master版本移植(9144ea8 on 29 Jun 2016 Git stats)

#####项目基本全部完成移植功能如下：
* 进入首页页面显示正常。

#####字符串包含带‘j’的，手动剪辑功能未实现，已提过工单确认：
* 目前无文本轮廓与下划线路径相交以对其进行剪辑的api（path.op(mUnderline, Path.Op.INTERSECT)）
 ![输入图片说明](images/screenshot.png)

##### 修改点：
* 修改原来path.op(mUnderline, Path.Op.INTERSECT)进行剪辑的部分，拆分字符串后单个划线
* 包名修改，原项目包名替换成"ohos"
* ohos项目中新增library，删除entry对应类